//
//  LoginViewController.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: - Private properties
    
    fileprivate lazy var emailInputView: CustomInputView = {
        let iv = CustomInputView(frame: .zero)
        iv.mainTextField.attributedPlaceholder = NSAttributedString(string: "Email".localize(), attributes: Constants.Attributes.blackPlaceholder)
        iv.mainTextField.autocapitalizationType = .none
        iv.mainTextField.autocorrectionType = .no
        iv.mainTextField.tfDelegate = self
        iv.mainTextField.clearButtonMode = .whileEditing
        iv.mainTextField.delegate = self
        iv.mainTextField.keyboardType = .emailAddress
        iv.mainTextField.returnKeyType = .next
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate lazy var passwordInputView: CustomInputView = {
        let iv = CustomInputView(frame: .zero)
        iv.mainTextField.attributedPlaceholder = NSAttributedString(string: "Password".localize(), attributes: Constants.Attributes.blackPlaceholder)
        iv.mainTextField.tfDelegate = self
        iv.mainTextField.clearButtonMode = .whileEditing
        iv.mainTextField.delegate = self
        iv.mainTextField.isSecureTextEntry = true
        iv.mainTextField.returnKeyType = .done
        iv.showSecureEntryImage = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate let forgotPasswordButton: UIButton = {
        let btn = UIButton(type: .custom)
        btn.addTarget(self, action: #selector(handleForgotPasswordButton), for: .touchUpInside)
        btn.setTitle("Forgot password?".localize(), for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = .systemFont(ofSize: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Private handlers
    
    @objc fileprivate func handleLogInButton() {
    }
    
    @objc fileprivate func handleForgotPasswordButton() {
    }
    
    // MARK: - Private functions
    
    fileprivate func setupView() {
        let height = view.frame.height
        let topBottomMargin = height * 0.05
        let inputHeightMultiplier: CGFloat = 0.08
        
        view.addSubview(emailInputView)
        emailInputView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topBottomMargin * 2).isActive = true
        emailInputView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        emailInputView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        emailInputView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: inputHeightMultiplier).isActive = true
        
        view.addSubview(passwordInputView)
        passwordInputView.topAnchor.constraint(equalTo: emailInputView.bottomAnchor, constant: topBottomMargin).isActive = true
        passwordInputView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        passwordInputView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        passwordInputView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: inputHeightMultiplier).isActive = true
        
//        view.addSubview(logInButton)
//        logInButton.topAnchor.constraint(equalTo: passwordInputView.bottomAnchor, constant: topBottomMargin * 2).isActive = true
//        logInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        logInButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.675).isActive = true
//        logInButton.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.105).isActive = true
//        
//        view.addSubview(forgotPasswordButton)
//        forgotPasswordButton.topAnchor.constraint(equalTo: logInButton.bottomAnchor, constant: topBottomMargin * 0.5).isActive = true
//        forgotPasswordButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        forgotPasswordButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9).isActive = true
    }
}

// MARK: - CustomTextFieldDelegate

extension LoginViewController: CustomTextFieldDelegate {
    func didSelectPreviousBarButton(_ textField: UITextField) {
        if textField === passwordInputView.mainTextField {
            emailInputView.mainTextField.becomeFirstResponder()
        }
    }
    
    func didSelectNextBarButton(_ textField: UITextField) {
        if textField === emailInputView.mainTextField {
            passwordInputView.mainTextField.becomeFirstResponder()
        }
    }
}

// MARK: - UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case emailInputView.mainTextField:
            emailInputView.mainTextField.previousBarButton.isEnabled = false
            break
        case passwordInputView.mainTextField:
            passwordInputView.mainTextField.nextBarButton.isEnabled = false
            break
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailInputView.mainTextField:
            passwordInputView.mainTextField.becomeFirstResponder()
            break
        case passwordInputView.mainTextField:
            let email = emailInputView.mainTextField.trimmedText
            let password = passwordInputView.mainTextField.trimmedText
            
            if !email.isEmpty && !password.isEmpty {
                handleLogInButton()
            } else {
                passwordInputView.mainTextField.resignFirstResponder()
            }
            
            break
        default:
            break
        }
        
        return true
    }
}
