//
//  Constants.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

struct Constants {
    
    struct Attributes {
        static let blackPlaceholder: [NSAttributedString.Key : Any] = [.font: UIFont.systemFont(ofSize: 18), .foregroundColor: UIColor.black.withAlphaComponent(0.5)]
    }
    
    struct UserDefaultsKeys {
        static let accessToken = "AccessToken"
    }
}
