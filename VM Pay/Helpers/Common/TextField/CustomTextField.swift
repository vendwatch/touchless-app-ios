//
//  CustomTextField.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

@objc protocol CustomTextFieldDelegate {
    @objc optional func didSelectPreviousBarButton(_ textField: UITextField)
    @objc optional func didSelectNextBarButton(_ textField: UITextField)
    @objc optional func didSelectCloseBarButton()
}

class CustomTextField: UITextField {
    
    // MARK: - Internal properties
    
    var allowPaste = true
    var tfDelegate: CustomTextFieldDelegate?
    var disableAllActions = false
    
    // Bar buttons are internal because can be hidden or disabled from another class
    lazy var previousBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "PreviousIcon"), style: .done, target: self, action: #selector(handlePreviousBarButton))
    lazy var nextBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "NextIcon"), style: .done, target: self, action: #selector(handleNextBarButton))
    lazy var closeBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "HideKeyboardIcon"), style: .done, target: self, action: #selector(handleCloseBarButton))
    
    // MARK: - Private properties
    
    fileprivate lazy var toolbar: UIToolbar = {
        let spaceBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.tintColor = .black
        toolbar.sizeToFit()
        toolbar.setItems([previousBarButton, nextBarButton, spaceBarButton, closeBarButton], animated: true)
        return toolbar
    }()
    
    fileprivate var tintedClearImage: UIImage?
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupToolbar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Overrides
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if disableAllActions {
            return false
        } else if (action == #selector(UIResponderStandardEditActions.paste(_:))) && !allowPaste {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tintClearImage()
    }
    
    // MARK: - Private handlers
    
    @objc fileprivate func handlePreviousBarButton(sender: UIBarButtonItem) {
        tfDelegate?.didSelectPreviousBarButton?(self)
    }
    
    @objc fileprivate func handleNextBarButton(sender: UIBarButtonItem) {
        tfDelegate?.didSelectNextBarButton?(self)
    }
    
    @objc fileprivate func handleCloseBarButton(sender: UIBarButtonItem) {
        tfDelegate?.didSelectCloseBarButton?()
        resignFirstResponder()
    }
    
    // MARK: - Private functions
    
    fileprivate func setupToolbar() {
        inputAccessoryView = toolbar
    }
    
    fileprivate func tintClearImage() {
        for view in subviews {
            if view is UIButton {
                guard let button = view as? UIButton else { continue }

                if let image = button.image(for: .highlighted) {
                    if tintedClearImage == nil {
                        tintedClearImage = tintImage(image: image, color: tintColor)
                    }

                    button.setImage(tintedClearImage, for: .normal)
                    button.setImage(tintedClearImage, for: .highlighted)
                }
            }
        }
    }
    
    fileprivate func tintImage(image: UIImage, color: UIColor) -> UIImage? {
        let size = image.size

        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)

        if let context = UIGraphicsGetCurrentContext() {
            let rect = CGRect(origin: .zero, size: CGSize(width: image.size.width, height: image.size.height))

            image.draw(at: .zero, blendMode: .normal, alpha: 1.0)

            context.setFillColor(color.cgColor)
            context.setBlendMode(.sourceIn)
            context.setAlpha(1.0)

            if let currentContext = UIGraphicsGetCurrentContext() {
                currentContext.fill(rect)

                let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()

                return tintedImage
            }
        }

        return nil
    }
}
