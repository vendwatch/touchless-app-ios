//
//  CustomInputView.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

protocol CustomInputViewDelegate {
    func didUpdateEntryText(_ text: String)
}

class CustomInputView: UIView {
    
    // MARK: - Internal properties
    
    var delegate: CustomInputViewDelegate?
    
    var image: UIImage? {
        didSet {
            mainImageView.isHidden = false
            mainImageView.image = image
        }
    }
    
    lazy var mainTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.addTarget(self, action: #selector(handleTextFieldChanges), for: .editingChanged)
        tf.font = .systemFont(ofSize: 18)
        tf.textColor = .black
        tf.tintColor = .black
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    var showSecureEntryImage = false {
        didSet {
            secureEntryButton.isHidden = !showSecureEntryImage
        }
    }
    
    /// The text shown in label
    var title: String? {
        didSet {
            mainLabel.text = title
        }
    }
    
    // MARK: - Private properties
    
    fileprivate let mainLabel: UILabel = {
        let lbl = UILabel()
        lbl.baselineAdjustment = .alignCenters
        lbl.font = .systemFont(ofSize: 14)
        lbl.textAlignment = .left
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    fileprivate let mainImageView: UIImageView = {
        let iv = UIImageView()
        iv.isHidden = true
        iv.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate lazy var secureEntryButton: UIButton = {
        let btn = UIButton()
        btn.addTarget(self, action: #selector(handleSecureEntryButton), for: .touchUpInside)
        btn.isHidden = true
        btn.setImage(#imageLiteral(resourceName: "ShowPasswordIcon").withRenderingMode(.alwaysTemplate), for: .normal)
        btn.setImage(#imageLiteral(resourceName: "HidePasswordIcon").withRenderingMode(.alwaysTemplate), for: .selected)
        btn.tintColor = .black
        btn.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    fileprivate let mainLineView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.cornerRadius = 0.01
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate var leftRightMargin: CGFloat = 0.0
    fileprivate var mainImageViewLeftConstraint: NSLayoutConstraint?
    fileprivate var mainTextFieldLeftConstraint: NSLayoutConstraint?
    fileprivate var mainTextFieldRightConstraint: NSLayoutConstraint?
    fileprivate var secureEntryButtonRightConstraint: NSLayoutConstraint?
    
    // MARK: - Overrides
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViewMargins()
        
        if image != nil {
            setupMainImageView()
        }
        
        if showSecureEntryImage {
            setupSecureEntryImageView()
        }
    }
    
    override func updateConstraints() {
        setupView()
        addTapGestureRecognizer()
        super.updateConstraints()
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private handlers
    
    @objc fileprivate func handleTapGestureRecognizer() {
        mainTextField.becomeFirstResponder()
    }
    
    @objc fileprivate func handleTextFieldChanges(sender: CustomTextField) {
        guard let text = sender.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        delegate?.didUpdateEntryText(text)
    }
    
    @objc fileprivate func handleSecureEntryButton(button: UIButton) {
        button.isSelected = !button.isSelected
        mainTextField.isSecureTextEntry = !button.isSelected
    }
    
    // MARK: - Private functions
    
    fileprivate func addTapGestureRecognizer() {
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer)))
    }
    
    fileprivate func setupMainImageView() {
        mainTextFieldLeftConstraint?.constant = leftRightMargin * 3
        mainTextField.layoutIfNeeded()
    }
    
    fileprivate func setupSecureEntryImageView() {
        mainTextFieldRightConstraint?.constant = -(leftRightMargin * 5)
        mainTextField.layoutIfNeeded()
    }
    
    fileprivate func setupView() {
        let mainWidthMultiplier: CGFloat = 0.7
        let topMargin: CGFloat = 10.0
        
        addSubview(mainLabel)
        mainLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        mainLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: mainWidthMultiplier).isActive = true
        
        addSubview(mainTextField)
        mainTextField.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: topMargin).isActive = true
        mainTextFieldLeftConstraint = mainTextField.leftAnchor.constraint(equalTo: leftAnchor)
        mainTextFieldRightConstraint = mainTextField.rightAnchor.constraint(equalTo: rightAnchor)
        mainTextFieldLeftConstraint?.isActive = true
        mainTextFieldRightConstraint?.isActive = true
        
        addSubview(mainImageView)
        mainImageView.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: topMargin).isActive = true
        mainImageView.widthAnchor.constraint(equalTo: mainTextField.heightAnchor).isActive = true
        mainImageView.heightAnchor.constraint(equalTo: mainTextField.heightAnchor).isActive = true
        mainImageViewLeftConstraint = mainImageView.leftAnchor.constraint(equalTo: leftAnchor)
        mainImageViewLeftConstraint?.isActive = true
        
        addSubview(secureEntryButton)
        secureEntryButton.topAnchor.constraint(equalTo: mainLabel.bottomAnchor, constant: topMargin).isActive = true
        secureEntryButton.widthAnchor.constraint(equalTo: mainTextField.heightAnchor).isActive = true
        secureEntryButton.heightAnchor.constraint(equalTo: mainTextField.heightAnchor).isActive = true
        secureEntryButtonRightConstraint = secureEntryButton.rightAnchor.constraint(equalTo: rightAnchor)
        secureEntryButtonRightConstraint?.isActive = true
        
        addSubview(mainLineView)
        mainLineView.topAnchor.constraint(equalTo: mainTextField.bottomAnchor, constant: topMargin).isActive = true
        mainLineView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        mainLineView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: mainWidthMultiplier).isActive = true
        mainLineView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
    
    fileprivate func setupViewMargins() {
        let width = bounds.width
        
        leftRightMargin = width * 0.05
        
        mainImageViewLeftConstraint?.constant = leftRightMargin
        secureEntryButtonRightConstraint?.constant = -leftRightMargin * 3
        mainTextFieldLeftConstraint?.constant = leftRightMargin * 3
        mainTextFieldRightConstraint?.constant = -leftRightMargin * 3
        
        mainImageView.layoutIfNeeded()
        mainTextField.layoutIfNeeded()
    }
}
