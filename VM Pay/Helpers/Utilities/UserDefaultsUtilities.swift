//
//  UserDefaultsUtilities.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import Foundation

class UserDefaultsUtilities {
    
    // MARK: - Private shared instance
    
    fileprivate static let defaults = UserDefaults.standard
    
    // MARK: - Properties saved in UserDefaults
    
    static var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.accessToken)
        }
        set (value) {
            UserDefaults.standard.set(value, forKey: Constants.UserDefaultsKeys.accessToken)
            UserDefaults.standard.synchronize()
        }
    }
    
    // MARK: - UserDefaults functions
    
    static func isLoggedIn() -> Bool {
        return accessToken != nil && accessToken != ""
    }
}
