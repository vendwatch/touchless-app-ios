//
//  UITextField+Utilities.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

extension UITextField {
    
    // MARK: - Additional properties
    
    var trimmedText: String {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
}
