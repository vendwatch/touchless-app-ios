//
//  String+Utilities.swift
//  VM Pay
//
//  Created by Dante Solorio on 18/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import Foundation

extension String {
    
    // MARK: - Additional functions
    
    /// Localize string
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
