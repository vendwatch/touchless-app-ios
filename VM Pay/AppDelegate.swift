//
//  AppDelegate.swift
//  VM Pay
//
//  Created by Dante Solorio on 16/06/20.
//  Copyright © 2020 Vendwatch Telematics. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {
    
    var window: UIWindow?
    
    // MARK: - Internal functions
    
    /// Setup root view controller
    func setupRootViewControllerOnWindow() {
        var rootViewController: UIViewController
        
        if UserDefaultsUtilities.isLoggedIn() {
            let tabBarController = CustomTabBarController()
            
            rootViewController = tabBarController
        } else {
            rootViewController = CustomNavigationController(rootViewController: LoginViewController())
        }
        
        changeRootViewController(rootViewController)
    }
    
    // MARK: - Private functions
    
    fileprivate func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.tintColor = .black
    }
    
    fileprivate func changeRootViewController(_ rootViewController: UIViewController) {
        window?.subviews.forEach({ $0.removeFromSuperview() })
        
        if window?.rootViewController == nil {
            window?.rootViewController = rootViewController
            window?.makeKeyAndVisible()
            return
        }
        
        if let snapshot = window?.snapshotView(afterScreenUpdates: true) {
            rootViewController.view.addSubview(snapshot)
            window?.rootViewController = rootViewController
            
            UIView.animate(withDuration: 0.5, animations: {
                snapshot.layer.opacity = 0.0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }) { (_) in
                snapshot.removeFromSuperview()
            }
        }
    }
}

extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        setupRootViewControllerOnWindow()
        return true
    }
}
